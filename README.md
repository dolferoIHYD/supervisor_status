# supervisor_status

Проверка состояния сервисов supervisor

### Установка

Склонировать в `/var/www/supervisor_status`, создать ссылку на unit-файл и "включить" его:

```
# ln -s /var/www/supervisor_status/supervisor_status.service /lib/systemd/system/supervisor_status.service
# systemctl daemon-reload
# systemctl enable supervisor_status
# systemctl start supervisor_status
```

### Использование

Сервис становится доступен на порту `44000`, запрос статуса - по пути `/<name>`.

Например, `http://localhost:44000/service`

Примеры ответов:

* Сервис работает
    ```json
    {
      "status": "ok"
    }
    ```
    
* Неверное имя сервиса
    ```json
    {
      "status": "error",
      "exception": "<Fault 10: 'BAD_NAME: name'>"
    }
    ```
    
* Сервис не запущен
    ```json
    {
      "status": "error",
      "exception": "AssertionError('Status is STOPPED')"
    }
    ```


