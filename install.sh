#!/bin/bash

ln -fs `pwd`/supervisor_status.service /lib/systemd/system/supervisor_status.service
sudo systemctl daemon-reload
sudo systemctl enable supervisor_status
sudo systemctl start supervisor_status
