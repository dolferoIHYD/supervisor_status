from bottle import route, response, run

from utils import get_status


@route('/<name>')
def get_status_view(name):
    try:
        status = get_status(name)
        assert status['statename'] == 'RUNNING', 'Status is {}'.format(status['STATENAME'])
        return {'status': 'ok'}
    except Exception as e:
        response.status = 400
        return {
            'status': 'error',
            'exception': repr(e),
        }


if __name__ == '__main__':
    run(host='0.0.0.0', port=44000)
